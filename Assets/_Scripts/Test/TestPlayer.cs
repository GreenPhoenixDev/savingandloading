﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayer : MonoBehaviour
{
	public int Health;
	[SerializeField] private float moveSpeed;
	[SerializeField] private TestData data;
	[SerializeField] private TestDataSO dataSO;

	void Start()
	{
		Debug.Log($"Hi, my name is {dataSO.Name}.\n Today is my birthday.\n Now i am {++dataSO.Age} years old\n");
	}

	void Update()
	{
		if (Input.GetKeyDown(KeyCode.S))
		{
			PlayerPrefs.SetString("name", data.Name); 
		}
		if (Input.GetKeyDown(KeyCode.P))
		{
			string n = PlayerPrefs.GetString("name");
			Debug.Log(n);
		}
	}
}