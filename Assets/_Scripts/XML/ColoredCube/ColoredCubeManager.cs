﻿using System.Collections.Generic;
using UnityEngine;

public class ColoredCubeManager : MonoBehaviour
{
	public SpawnMode spawnMode = SpawnMode.Random;
	public Database<ColoredCubeData> Database = new Database<ColoredCubeData>();

	[SerializeField] private string pathToFileEnd = "/XML/coloredCube_data.xml";
	public string PathToFile
	{
		get { return pathToFile; }
		set { pathToFile = value; }
	}
	private string pathToFile;

	private void Awake()
	{
		pathToFile = Application.streamingAssetsPath + pathToFileEnd;
	}

	public void SetSpawnMode(int idx)
	{
		spawnMode = (SpawnMode)idx;
	}

	public void SpawnColoredCubes()
	{
		foreach (Transform child in transform)
		{
			Destroy(child.gameObject);
		}

		ColoredCubeSpawner spawner = GetComponent<ColoredCubeSpawner>();
		spawner.Spawn(spawnMode);
	}

	public void SaveColoredCubes()
	{
		List<ColoredCubeData> dataList = new List<ColoredCubeData>();
		foreach (Transform child in transform)
		{
			dataList.Add(child.GetComponent<ColoredCube>().Data);
		}
		XMLManager<ColoredCubeData>.SaveData(dataList, pathToFile, Database);
	}
}

public enum SpawnMode { Random, XML }