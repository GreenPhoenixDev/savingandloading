﻿using System.Collections.Generic;
using System.Text;
using UnityEngine;

public class ColoredCubeSpawner : MonoBehaviour
{
	[SerializeField] private int numOfRandomCubes = 5;
	[SerializeField] private ColoredCubeManager ccm;

	public void Spawn(SpawnMode spawnMode)
	{
		SpawnCubes(Prepare(spawnMode));
	}

	public List<ColoredCubeData> Prepare(SpawnMode spawnMode)
	{
		List<ColoredCubeData> dataList;

		if (spawnMode == SpawnMode.Random)
			dataList = GetRandomData(numOfRandomCubes);
		else
			dataList = XMLManager<ColoredCubeData>.LoadData(ccm.PathToFile, ccm.Database);

		return dataList;
	}

	public void SpawnCubes(List<ColoredCubeData> dataList)
	{
		for (int i = 0; i < dataList.Count; i++)
		{
			GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
			ColoredCube colCube = cube.AddComponent<ColoredCube>();
			colCube.Data = dataList[i];
			colCube.Init();
			colCube.transform.parent = transform;
		}
	}

	#region Get Random Values
	List<ColoredCubeData> GetRandomData(int count)
	{
		List<ColoredCubeData> dataList = new List<ColoredCubeData>(count);

		for (int i = 0; i < count; i++)
		{
			dataList.Add(new ColoredCubeData(GetRandomColor(), GetRandomVector3(3f, false), GetRandomVector3(1.5f, true), GetRandomString(7)));
		}

		return dataList;
	}
	Color GetRandomColor()
	{
		return Random.ColorHSV(0f, 1f, 0.4f, 0.7f, 0.7f, 0.8f);
	}
	Vector3 GetRandomVector3(float range, bool onlyPositive)
	{
		if (onlyPositive)
			return ((Random.insideUnitSphere * 0.5f) + Vector3.one) * range;
		else
			return Random.insideUnitSphere * range;
	}
	string GetRandomString(int length)
	{
		string pool = "abcdefghijklmnopqrstuvwxyz";
		StringBuilder sb = new StringBuilder();
		int seed = Random.Range(0, 500);
		System.Random random = new System.Random(seed);

		for (int i = 0; i < length; i++)
		{
			var c = pool[random.Next(0, pool.Length)];
			sb.Append(c);
		}

		return sb.ToString();
	}
	#endregion
}
