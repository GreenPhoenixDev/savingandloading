﻿using UnityEngine;

public class ColoredCubeData
{
	public Color Color;
	public Vector3 Position;
	public Vector3 Scale;
	public string Name;

	public ColoredCubeData(Color color, Vector3 position, Vector3 scale, string name)
	{
		Color = color;
		Position = position;
		Scale = scale;
		Name = name;
	}
	public ColoredCubeData() { }
}
