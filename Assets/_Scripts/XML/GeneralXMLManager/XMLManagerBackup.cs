﻿//using System.Collections.Generic;
//using System.Xml.Serialization;
//using System.Text;
//using System.IO;
//using UnityEngine;

//public class XMLManagerBackup : Singleton<XMLManagerBackup>
//{
//	private string pathToFile;
//	public Database DataBase;

//	void Awake()
//	{
//		pathToFile = Application.streamingAssetsPath + "/XML/coloredCube_data.xml";
//	}

//	public List<ColoredCubeData> LoadColoredCubes()
//	{
//		if (File.Exists(pathToFile))
//		{
//			using (StreamReader reader = new StreamReader(pathToFile))
//			{
//				XmlSerializer serializer = new XmlSerializer(typeof(Database));
//				DataBase = serializer.Deserialize(reader) as Database;
//				return DataBase.coloredCubeDataList;
//			}
//		}
//		else { return null; }
//	}

//	public void SaveColoredCubes(List<ColoredCubeData> dataList)
//	{
//		DataBase.coloredCubeDataList = dataList;
//		Encoding encoding = Encoding.GetEncoding("UTF-8");

//		using (StreamWriter writer = new StreamWriter(pathToFile, false, encoding))
//		{
//			XmlSerializer serializer = new XmlSerializer(typeof(Database));
//			serializer.Serialize(writer, DataBase);
//		}
//	}
//}

//[System.Serializable]
//public class Database
//{
//	[XmlArray("ColoredCubes")] public List<ColoredCubeData> coloredCubeDataList = new List<ColoredCubeData>();
//}
