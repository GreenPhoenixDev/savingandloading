﻿using UnityEngine;

public class Singleton<T> : MonoBehaviour where T : Singleton<T>
{
	private static T instance;
	public static T Instance
	{
		get
		{
			if (instance == null)
			{
				instance = (T)FindObjectOfType(typeof(T));

				if (instance != null)
					DontDestroyOnLoad(instance.gameObject);
			}

			if (instance == null)
			{
				GameObject singleton = new GameObject();
				instance = singleton.AddComponent<T>();
				singleton.name = "(Singleton) " + typeof(T).ToString();
				DontDestroyOnLoad(instance.gameObject);
			}

			return instance;
		}
	}

	void Awake()
	{
		if (instance != null)
		{
			Destroy(gameObject);
			return;
		}

		DontDestroyOnLoad(gameObject);
		instance = (T)this;
	}
}
