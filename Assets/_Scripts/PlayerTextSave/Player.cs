﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System;

public class Player : MonoBehaviour
{
	[SerializeField] private PlayerDefaultDataSO defaultData;
	[SerializeField] private Transform weaponSpawnPoint;
	[SerializeField] private string SUBPATH = "/PlayerData.txt";
	private const char DELIMITER = '|';
	private const string FLOATRANGE = "0.000";
	private Weapon equippedWeapon;
	private string playerName;
	private Vector3 spawnPos;
	private int currentHealth;
	private int level;
	private int score;
	[SerializeField] private bool useSavedData = false;
	private List<string[]> lines = new List<string[]>();

	#region MonoBehaviour
	void Start()
	{
		if (!useSavedData)
			SetupPlayer();
		else
			SetupPlayerFromFile();
	}
	void Update()
	{
		if (Input.GetKeyDown(KeyCode.S))
			SavePlayerDataToFile();
	}
	#endregion

	#region PlayerData
	void SetupPlayer()
	{
		spawnPos = defaultData.SpawnPos;
		playerName = defaultData.PlayerName;
		currentHealth = defaultData.CurrentHealth;
		level = defaultData.Level;
		score = defaultData.Score;
		transform.position = spawnPos;
		equippedWeapon = Instantiate(defaultData.EquippedWeapon, weaponSpawnPoint.position, weaponSpawnPoint.rotation);
		equippedWeapon.transform.parent = weaponSpawnPoint;
	}
	void SetupPlayerFromFile()
	{
		using (StreamReader reader = new StreamReader(Application.streamingAssetsPath + SUBPATH))
		{
			// weaponStats
			equippedWeapon = Instantiate(defaultData.EquippedWeapon, weaponSpawnPoint.position, weaponSpawnPoint.rotation);
			equippedWeapon.transform.parent = weaponSpawnPoint;

			string[] tokens = reader.ReadLine().Split(DELIMITER);
			equippedWeapon.Color = new Color(float.Parse(tokens[1]), float.Parse(tokens[2]), float.Parse(tokens[3]));
			equippedWeapon.Name = reader.ReadLine().Split(DELIMITER)[1];
			equippedWeapon.ATKDamage = int.Parse(reader.ReadLine().Split(DELIMITER)[1]);
			equippedWeapon.Durability = int.Parse(reader.ReadLine().Split(DELIMITER)[1]);
			equippedWeapon.CurrentDurability = int.Parse(reader.ReadLine().Split(DELIMITER)[1]);

			//playerStats
			playerName = reader.ReadLine().Split(DELIMITER)[1];
			gameObject.name = playerName;
			tokens = reader.ReadLine().Split(DELIMITER);
			spawnPos = new Vector3(float.Parse(tokens[1]), float.Parse(tokens[2]), float.Parse(tokens[3]));
			currentHealth = int.Parse(reader.ReadLine().Split(DELIMITER)[1]);
			level = int.Parse(reader.ReadLine().Split(DELIMITER)[1]);
			score = int.Parse(reader.ReadLine().Split(DELIMITER)[1]);

			transform.position = spawnPos;

			//int[] blockRow = Array.ConvertAll<string, int>(tokens, int.Parse);
			//blocks.Add(blockRow);
		}
	}
	void SavePlayerDataToFile()
	{
		string[] lines = new string[10];
		// Weapon
		string r = equippedWeapon.Color.r.ToString(FLOATRANGE);
		string g = equippedWeapon.Color.g.ToString(FLOATRANGE);
		string b = equippedWeapon.Color.b.ToString(FLOATRANGE);
		lines[0] = $"vSpawnPos{DELIMITER}{r}{DELIMITER}{g}{DELIMITER}{b}";
		lines[1] = "sWeaponName" + DELIMITER + equippedWeapon.Name;
		lines[2] = "iWeaponDamage" + DELIMITER + equippedWeapon.ATKDamage.ToString();
		lines[3] = "iWeaponDur" + DELIMITER + equippedWeapon.Durability.ToString();
		lines[4] = "iCurWeaponDur" + DELIMITER + equippedWeapon.CurrentDurability.ToString();
		// Player
		lines[5] = "sPlayername" + DELIMITER + playerName;
		string x = spawnPos.x.ToString(FLOATRANGE);
		string y = spawnPos.y.ToString(FLOATRANGE);
		string z = spawnPos.z.ToString(FLOATRANGE);
		lines[6] = $"vSpawnPos{DELIMITER}{x}{DELIMITER}{y}{DELIMITER}{z}";
		lines[7] = "iCurrentHealth" + DELIMITER + currentHealth.ToString();
		lines[8] = "iLevel" + DELIMITER + level.ToString();
		lines[9] = "iScore" + DELIMITER + score.ToString();

		using (StreamWriter writer = new StreamWriter(Application.streamingAssetsPath + SUBPATH))
		{
			foreach (string line in lines)
			{
				writer.WriteLine(line);
			}
		}
		Debug.Log("FINISHED SAVING!");
	}
	#endregion

	#region Utility
	string ConvertFloatNotation(string input)
	{
		var splitInput = input.Split('.');
		//var splitInput = input.Split('.', 'f');
		for (int i = 0; i < splitInput.Length; i++)
		{
			Debug.Log(splitInput[i].ToString());
		}
		return splitInput[0] + "," + splitInput[1].Split('f');
	}
	#endregion
}