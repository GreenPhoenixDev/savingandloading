﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class BinarySerializer : MonoBehaviour
{
	public void SaveGameData(string path, string filename, GameSaveData data)
	{
		if (File.Exists(path + filename))
		{
			Debug.LogWarning("Overriding file!");
		}

		using (FileStream stream = File.Create(path + filename))
		{
			BinaryFormatter bf = new BinaryFormatter();
			bf.Serialize(stream, data);
		}
	}

	public GameSaveData LoadGameData(string path, string filename)
	{
		if (!File.Exists(path + filename))
		{
			Debug.LogError("File does NOT exist!");
			return null;
		}
		else
		{
			using(FileStream stream = new FileStream(path + filename, FileMode.Open))
			{
				BinaryFormatter bf = new BinaryFormatter();
				return (GameSaveData)bf.Deserialize(stream);
			}
		}
	}
}

[System.Serializable]
public class GameSaveData
{
	public string PlayerName;
	public SerializableVector3 PlayerPosition;

	public int CurrentLevel => currentLevel;
	[SerializeField] private int currentLevel;

	public GameSaveData(int _currentLevel, string _playerName, Vector3 _playerPosition)
	{
		currentLevel = _currentLevel;
		PlayerName = _playerName;
		PlayerPosition = new SerializableVector3(_playerPosition);
	}
}

[System.Serializable]
public struct SerializableVector3
{
	public float x;
	public float y;
	public float z;

	public SerializableVector3(Vector3 v)
	{
		x = v.x;
		y = v.y;
		z = v.z;
	}

	public Vector3 ToVector3()
	{
		return new Vector3(x, y, z);
	}
}