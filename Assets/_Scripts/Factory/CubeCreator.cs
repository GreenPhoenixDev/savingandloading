﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CubeCreator : IFactory
{
	private Vector3 pos;
	private Quaternion rot;
	private Vector3 scale;
	private GameObject cubePre;

	public CubeCreator(Vector3 _pos, Quaternion _rotation, Vector3 _scale, GameObject _cubePre)
	{
		pos = _pos;
		rot = _rotation;
		scale = _scale;
		cubePre = _cubePre;
	}

	public void Create()
	{
		GameObject go = GameObject.Instantiate(cubePre);
		go.GetComponent<ISpawnableGameObject>().SetValues(pos, rot, scale);
	}
}
