﻿using UnityEngine;

// Command pattern
public interface ICommand1
{
	void Execute();
	void Undo();
}

public interface IMoveable1
{
	void Move(Vector3 dir);
}

public interface IScalable1
{
	void Scale(float factor);
}

public interface ITransformable1 : IMoveable1, IScalable1
{

}

// Factory Pattern
public interface IFactory
{
	void Create();
}

public interface ISpawnableGameObject
{
	void SetValues(Vector3 pos, Quaternion rotation, Vector3 scale);
}